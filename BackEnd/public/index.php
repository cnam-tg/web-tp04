<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;


require '../vendor/autoload.php';

$app = new \Slim\App;

const SECRET = "mykey123456789";
$jwt = new \Slim\Middleware\JwtAuthentication([
    "path" => "/api",
    "secure" => false,
    "passthrough" => ["/api/users/login","/api/users/register"],
    "secret" => JWT_SECRET,
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');


$app->get('/api/products',function ($resquest,$response,$args)
 {
    $url="../src/products.json";
    $produits=file_get_contents($url);

    return $response->write($produits)
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader("Access-Control-Allow-Origin", "*")
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->post('/api/users/login', function (Request $request, Response $response, array $args) use ($app) {
    $data = $request->getParsedBody();
    $login = $body['login'];
    $password = $body['password']; 

    $issuedAt = time();
    $expirationTime = $issuedAt + 6000; // jwt valid for 60 seconds from the issued time
    $payload = array(
    // id a stocker lors de l'utilisation de la bdd
    'login' => $login,
    'iat' => $issuedAt,
    'exp' => $expirationTime
    );
    
    $token_jwt = JWT::encode($payload,JWT_SECRET, "HS256");
     $data = array("token" => "Bearer {$token_jwt}");
      //return new Response(json_encode($data), 200, array('Content-Type' => 'application/json'));
                  return $response
                  ->withHeader("Content-Type", "application/json")
                  ->withJson($data);
    });


// Run app
$app->run();

