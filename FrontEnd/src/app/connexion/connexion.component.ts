import { Component, OnInit, ɵɵcontainerRefreshEnd } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthentificationService } from '../service/authentification.service';
import { Router } from '@angular/router';
import { LoadProducts } from '../store/actions/product.action';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  loginForm: FormGroup;
  alreadyLoggedIn = false;
  errors ="";


  login: any;
  password: any;


  constructor(
    private formBuilder: FormBuilder,
    private authentificationService: AuthentificationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.load();
  }


  connect() {
    console.log(this.login);
    console.log(this.password);
    
    this.authentificationService.login(this.login, this.password).subscribe(success => {
      localStorage.setItem('jwt_token', success.token);
      this.load();
    });
  }

  load() {
    this.alreadyLoggedIn = this.authentificationService.isLoggedIn ? true : false;
  }

}
